# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class RegistryObjectStorage < Default
          def configuration
            Runtime::Env.require_aws_s3_environment!

            <<~OMNIBUS
              registry['storage'] = { 's3' => { 'accesskey' => '#{Runtime::Env.aws_s3_key_id}', 'secretkey' => '#{Runtime::Env.aws_s3_access_key}', 'bucket' => '#{Runtime::Env.aws_s3_bucket_name}', 'region' => '#{Runtime::Env.aws_s3_region}' } }
            OMNIBUS
          end
        end
      end
    end
  end
end
